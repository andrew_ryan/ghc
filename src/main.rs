#[allow(warnings)]
pub fn find(name: &str) -> Vec<String> {
    use walkdir::WalkDir;
    let mut files = vec![];
    for entry in WalkDir::new("./").into_iter().filter_map(|e| e.ok()) {
        if entry.path().is_file() {
            let path = format!("{}", entry.path().display());
            if entry.path().is_file() && std::fs::read_to_string(entry.path()).is_ok() {
                if let Ok(file_0) = std::fs::read_to_string(entry.path()) {
                    if !file_0.contains("https://ghproxy.com/https://github.com")
                        && file_0.contains("https://github.com")
                    {
                        if path.ends_with(name) {
                            files.push(path);
                        }
                    }
                }
            }
        }
    }
    files
}
pub fn all_file() -> Vec<String> {
    use walkdir::WalkDir;
    let mut files = vec![];
    for entry in WalkDir::new("./").into_iter().filter_map(|e| e.ok()) {
        if entry.path().is_file() {
            let path = format!("{}", entry.path().display());
            if entry.path().is_file() && std::fs::read_to_string(entry.path()).is_ok() {
                if let Ok(file_0) = std::fs::read_to_string(entry.path()) {
                    if !file_0.contains("https://ghproxy.com/https://github.com")
                        && file_0.contains("https://github.com")
                    {
                        files.push(path);
                    }
                }
            }
        }
    }
    files
}

#[allow(warnings)]
pub fn replace(path: String) {
    if let Ok(file_0) = std::fs::read_to_string(&path) {
        let mut file_0 = file_0.to_string();
        if !file_0.contains("https://ghproxy.com/https://github.com")
            && file_0.contains("https://github.com")
        {
            std::fs::write("./.ghc.log", "").expect("create file error");
            println!("replace https://github.com in {}", &path);
            let new_file = file_0.replace(
                "https://github.com",
                "https://ghproxy.com/https://github.com",
            );
            file_0 = new_file;
        }
        std::fs::write(&path, file_0.to_string()).expect("write new file error");
        easy_file::append_to_file!("./.ghc.log", format!("{}\n", path.to_string()).as_bytes())
            .expect("write log file error");
    }
}

fn main() {
    use cok::cli::Cli;
    let json = include_str!("../json/cli.json");
    Cli::load_from_json(json);
    let args = Cli::args();
    match args.get(0) {
        Some(r) => {
            if r != "-h" || r != "--help" {
                if r == "--all" || r == "-a" {
                    println!("replace all files");
                    let files = all_file();
                    for path in &files {
                        replace(path.to_string());
                    }
                } else if r == "--recover" || r == "-r" {
                    if let Ok(r) = std::fs::read_to_string("./.ghc.log") {
                        let paths = r.split("\n").filter(|s| !s.is_empty()).collect::<Vec<_>>();
                        for p in paths.iter() {
                            if let Ok(r) = std::fs::read_to_string(p) {
                                println!("recover {}", p);
                                let rr = r.replace(
                                    "https://ghproxy.com/https://github.com",
                                    "https://github.com",
                                );
                                std::fs::write(p, rr).expect("recover error");
                            }
                        }
                    }
                } else {
                    for a in args {
                        let result = find(&a);
                        for path in &result {
                            replace(path.to_string());
                        }
                    }
                }
            }
        }
        None => {}
    }
}
