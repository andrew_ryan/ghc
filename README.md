# Cli for efficient development in china
[![Crates.io](https://img.shields.io/crates/v/ghc.svg)](https://crates.io/crates/ghc)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/ghc)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/ghc/-/raw/master/LICENSE)
```
ghc 0.1.13
Anonymous <dnrops@anonymous.com>
Cli for efficient development in china
Example:
ghc .js .toml => in current dir find all *.js *.toml and if contain https://github.com repleace to https://ghproxy.com/https://github.com
ghc config => in current dir find all config files and if contain https://github.com repleace to https://ghproxy.com/https://github.com

USAGE:
        OPTION          REQUIRED        ABOUT
ghc     -a --all        false           Find all files that contain https://github.com repleace to https://ghproxy.
ghc     -r --recover    false           recover the files that repleaced to original
ghc     -h --help                       Prints help information
```

## Example
```
# in current dir find all Cargo.toml and if cointain https://github.com repleace https://github.com to https://ghproxy.com/https://github.com
ghc Cargo.toml 
```

## install
```
cargo install ghc
```

## download bin
``` 
#linux
wget https://gitlab.com/andrew_ryan/ghc/-/raw/master/bin/ghc
#windows
wget https://gitlab.com/andrew_ryan/ghc/-/raw/master/bin/ghc.exe
```
